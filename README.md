# Description

This plugin will compile your Haste files to the right location for cordova to package.

Add this plugin to your Cordova project.

    cordova plugin add https://bitbucket.org/sumitraja/cordova-plugin-haste-lang

All .hs files must go in haste/ in the root of the project

## Prerequisites

You must install [Haste](http://haste-lang.org/) and use haste-cabal to install the required haste packages