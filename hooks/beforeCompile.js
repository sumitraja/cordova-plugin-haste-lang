#!/usr/bin/env node

var execSync = require('child_process').execSync;
if(execSync == undefined) {
  throw Error("child_process.execSync not found. Is nodejs up to date?");
}
var fs = require('fs');

var hastePath = 'haste'

var files = getHsFiles(hastePath);
console.log("Processing files: " + files);
execSync("hastec --outdir=" + hastePath + " --out=www/js/main.js " + files.join(" "));

function endsWith(str, suffix) {
    return str.indexOf(suffix, str.length - suffix.length) !== -1;
}

function getHsFiles (dir) {
  var files = [];
  var fsfiles = fs.readdirSync(dir);
  for (var i in fsfiles) {
    var name = dir + "/" + fsfiles[i];
    if(!fs.statSync(name).isDirectory() && endsWith(name, ".hs")) {
      files.push(name);
    }
  }
  return files;
}
